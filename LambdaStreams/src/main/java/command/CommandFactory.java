package command;

public interface CommandFactory {
    public Command getCommand(Integer commandName);
}
