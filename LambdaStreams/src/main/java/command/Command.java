package command;

@FunctionalInterface
public interface Command {
    public void execute(String name);
}
