package command.impl;

import command.Command;

public class FrenchGreetingCommand implements Command {

    @Override
    public void execute(String name) {
        System.out.println("Bonjour " + name);
    }
}
