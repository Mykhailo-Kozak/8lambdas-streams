package command.impl;

public class CommandCodes {
    public static final Integer UA_CODE = 1;
    public static final Integer EN_CODE = 2;
    public static final Integer FR_CODE = 3;
    public static final Integer PL_CODE = 4;
}
