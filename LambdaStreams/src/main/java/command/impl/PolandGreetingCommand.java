package command.impl;

public class PolandGreetingCommand {
    public void sayHello(String name) {
        System.out.println("Dzień dobry " + name);
    }
}
