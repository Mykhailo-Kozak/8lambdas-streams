package command.impl;

import command.Command;
import command.CommandFactory;

import java.util.HashMap;
import java.util.Map;

import static command.impl.CommandCodes.*;

public class GreetingCommandFactory implements CommandFactory {

    private Map<Integer, Command> commandMap;


    public GreetingCommandFactory() {

        commandMap = new HashMap<>();
        commandMap.put(UA_CODE, name -> System.out.println("Привіт " + name));
        commandMap.put(EN_CODE, new Command() {
            @Override
            public void execute(String name) {
                System.out.println("Hello " + name);
            }
        });
        FrenchGreetingCommand frenchGreetingCommand = new FrenchGreetingCommand();
        commandMap.put(FR_CODE, frenchGreetingCommand);
        PolandGreetingCommand polandGreetingCommand = new PolandGreetingCommand();
        commandMap.put(PL_CODE, polandGreetingCommand::sayHello);
    }

    @Override
    public Command getCommand(Integer commandCode) {
        if (commandMap.containsKey(commandCode))
            return commandMap.get(commandCode);
        else
            return name -> System.out.println("Sorry " + name + ", but greeting is unavaible now");
    }
}
