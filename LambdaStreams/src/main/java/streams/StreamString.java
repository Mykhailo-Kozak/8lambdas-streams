package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class StreamString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            list.add(scanner.nextLine());
        }

        System.out.println(list.stream().count());
        System.out.println(list.stream().distinct().count());
        System.out.println(list.stream().distinct().sorted().collect(Collectors.toList()));
        System.out.println(list.stream().map( s -> {return s.toLowerCase(); }).collect(Collectors.toList()));
        //System.out.println(list.stream().map());
        /*for (String s: list) {
            System.out.println(s);
        }*/
    }
}
