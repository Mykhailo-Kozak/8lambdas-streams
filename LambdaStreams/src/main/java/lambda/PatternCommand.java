package lambda;

import command.Command;
import command.CommandFactory;
import command.impl.GreetingCommandFactory;

import java.util.Scanner;

import static command.impl.CommandCodes.*;

public class PatternCommand {
    public static void main(String[] args) {

        CommandFactory commandFactory = new GreetingCommandFactory();

        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("#######################");
            System.out.println("Choose language code:");
            System.out.println(UA_CODE + ".Ukrainian");
            System.out.println(EN_CODE + ".English");
            System.out.println(FR_CODE + ".French");
            System.out.println(PL_CODE + ".Polish");
            int choise;
            if (scanner.hasNextInt())
                choise = scanner.nextInt();
            else {
                System.out.println("Wrong value, try again!");
                System.out.println("#######################");
                continue;
            }
            Command command = commandFactory.getCommand(choise);
            command.execute("Михайло");
        }
    }
}
