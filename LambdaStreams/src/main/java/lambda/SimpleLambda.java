package lambda;

public class SimpleLambda {
    public static void main(String[] args) {
        SimpleLambda simpleLambda = new SimpleLambda();

        MyLambda lambda = (a, b, c) -> simpleLambda.maxValue(a, b, c);
        MyLambda lambda1 = (a, b, c) -> (a + b + c) / 3;
        System.out.println(lambda.fun1(5, 10, 11));
        System.out.println(lambda1.fun1(4, 8, 6));
    }

    private int maxValue(int a, int b, int c) {
        int max = a;
        if (a > b && a > c)
            max = a;
        if (b > a && b > c)
            max = b;
        if (c > a && c > b)
            max = c;
        return max;
    }

    @FunctionalInterface
    interface MyLambda {
        int fun1(int a, int b, int c);
    }
}
